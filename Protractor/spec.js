describe('TaskWorld Automation Project', function () {

	var username = "vinay.skp40@gmail.com";
	var password = "TaskTest123";
	var projectName = "Task-AutomationProject";
	var projectDescription = "This project is created for interview purpose";
	var taskList = "SampleTaskList";
	var taskListDescription = "This is the test task";

	it('Login', function () {

		console.log("Executing the Login test");

		browser.manage().timeouts().implicitlyWait(10000);

		browser.ignoreSynchronization = true;

		browser.get('https://asia-enterprise.taskworld.com/login');

		browser.driver.manage().window().maximize();

		browser.sleep(5000);

		SendInput('//input[@type="email"]', username);

		SendInput('//input[@type="password"]', password);

		ClickElement('//button[@type="submit"]');

	});

	it('Creating a Project', function () {

		console.log("Creating a Test Project");

		ClickElement('//i[@class="tw-icon tw-icon-add --name_add"]');

		SendInput('//input[@name="project-name"]',projectName);

		SendInput('//input[@name="description"]',projectDescription);

		ClickElement('//span[text()="Next: Choose a template"]');
		
		ClickElement('//div[text()="Create Project"]');

	});

	it('Creating a Task List', function () {

		console.log("Creating a Task List");

		element(by.xpath('//input[@placeholder="Tasklist title"]')).sendKeys(taskList, protractor.Key.ENTER);

		ClickElement('//div[@class="tw-pre-created-tasklist --column"]/i');

		ClickElement('//div[text()="'.concat(taskList, '"]/parent::div/parent::div/parent::div/following-sibling::div/div[@role="button"]'));

		SendInput('//textarea[@placeholder="Create a new task"]',taskListDescription);

		ClickElement('//span[text()="Create"]');

		ClickElement('//span[text()="'.concat(taskListDescription, '"]'));

		ClickElement('(//i[@class="tw-icon tw-icon-close --name_close"])[1]');

		ClickElement('//span[text()="'.concat(taskListDescription, '"]/preceding-sibling::div/div[@role="button"]'));

		ClickElement('//span[text()="'.concat(taskListDescription, '"]'));

		ClickElement('(//i[@class="tw-icon tw-icon-close --name_close"])[1]');

	});
});

var SendInput = function (locator, data) {
	browser.wait(function () {
		return element(by.xpath(locator)).isDisplayed();
	}, 300000).then(function () {
		element(by.xpath(locator)).sendKeys(data);
	}); ;
}

var ClickElement = function (locator) {
	browser.wait(function () {
		return element(by.xpath(locator)).isDisplayed();
	}, 300000).then(function () {
		element(by.xpath(locator)).click();
	}); ;
}
